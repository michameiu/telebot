'use strict'

const Telegram = require('telegram-node-bot')
const us = require("./users")
const room = require("./roometo")
const TelegramBaseController = Telegram.TelegramBaseController
const TextCommand = Telegram.TextCommand
const RegexpCommand = Telegram.RegexpCommand
const numeral = require('numeral');
const payments = require("./payment")
numeral.defaultFormat('0,0');
const tg = new Telegram.Telegram('454762986:AAHM_2DX04NXbfkzkejkKJG7LVXkVe2yRro',
    {
        workers: 1,
        webAdmin: {
            disable: true,
            url: process.env.APP_URL || 'https://botmeiu.herokuapp.com',
            port: process.env.PORT || 8888
        }
    })
var apiai = require('apiai');
const uuid = require('uuid');
const projectId = "meiu-9671a"
const sessionId = uuid.v4();
const dialogflow = require('dialogflow')
const sessionClient = new dialogflow.SessionsClient();

const kplc = require("./kplc")
var app = apiai("d3c23c8a12ad481d874516e023e1ac8e");


var request = require('request');

var reg = /(\w{3,5}\/\w{5}[\/\w{4}]*)/i
var regcmd = /(\w{3,5}\/\w{5}[\/\w{4}]*)/i
var kplcregx = /kplc (\d{5,11})/i

const tuk = require("./pynode")

class PingController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    pingHandler($) {
        console.log("Phone command ", $.message.text.replace("/phone", ""))
        var phone = $.message.text.replace("/phone", "").replace(" ", "")
        $.setUserSession('phone', phone)
            .then(() => {
                return $.getUserSession('phone')
            })
            .then(data => {
                console.log(data)
            })
        console.log($.userId)
        // us.getAllUsers($, results => {
        //     console.log(results)
        // })
        const users = ["384922104"]
        const imgUrl = "https://api.roometo.com/media/CACHE/images/b4b64fc9-d9b1-4376-a52f-2a4739aaa6cb/8a8fcd3a56b4c779a5705eac3358ba85.JPG"
        // $.sendPhoto(imgUrl)
        $.sendMessage("<a href='https://www.youtube.com/watch?v=7OeMjLVCGJs'>Youtube</a>", { parse_mode: "HTML" })

        $.sendMessage('pong')
    }

    get routes() {
        return {
            'pingCommand': 'pingHandler'
        }
    }
}
class OtherWiseController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    handle($) {
        var message = $.message.text
        $.getChatMember($.userId).then(daa => {
            // console.log(daa.user)
            us.saveUser($, daa.user, dat => {
                // console.log(dat)
            });
        })

        console.log(message, $.userId);
        if (message == "/start") {
            //Welcome messaga
            sendWelcomeMessage($)
            return
        }
        $.getUserSession('phone').then(data => {
            // console.log(data)
        })

        getapiai($, message, resp => {
            handleapiairesponse($, message, resp)
        }, error => {
            $.sendMessage("Error")
        });

    }
}
function sendWelcomeMessage($) {
    let message = `
 *Welcome* 😊

Am Meiu a conversation bot.
The bot can remember your name, for TuK students it can also remember your student id and many more details. 

*Features*
- Jokes
- Engaging conversations
- Get KPLC bill for postpaid meters.
- Searching for hostels listed on [Roometo](https://roometo.com).
- Get fees balance. (TuK students only.)

*Smart Examples to send (Meiu is still learning so ...😁)*
- Wamlambez 
- what's my fee balance?
- search hostels in ngara for 5000 
- 2 sharing hostels in westlands
- My name is Agnes ?
- What's your name ?
- Where were your born ?
- Tell me a joke

*Easy peasy examples you can send Meiu*
/tuk 113/00421 
/kplc 1123332 - (1123332 is the meter number) 

*Enjoy*
MeiuTeam.
    `
    $.sendMessage(message, { parse_mode: 'Markdown' })


}
class TukController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    start($) {
        // $.sendMessage('Hello! please iclude your tuk student id , eg /tuk 113/00421')

        /* Sendind a file */
        // var file="pdf/MWANGI_113_00421.pdf"
        // $.sendDocument({ path: file})

        kplc.getKplcBill(34617320, data => {
            $.sendMessage(JSON.stringify(data))
        }, error => {
            $.sendMessage(error)
        });

    }
    get routes() {
        return {
            'startCommand': 'start'
        }
    }
}
class KplcController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    start($) {
        // $.sendMessage('Hello! please iclude your tuk student id , eg /tuk 113/00421')
        /* Sendind a file */
        // var file="pdf/MWANGI_113_00421.pdf"
        // $.sendDocument({ path: file})
        var id = $.message.text.match(kplcregx)[1]
        // $.sendMessage("A sec ...")
        var feature = "kplc"
        us.updateFeatureUsed($, feature, data => {
            console.log(`${feature} used: `, data)
        });
        kplc.getKplcBill(id, data => {
            var billlen = data.colBills.length
            var kwhlen = data.collUsagesOutput.length
            var lastmonthbill = billlen > 0 ? data.colBills[0] : "Uknown"
            var lastmonthkwh = data.collUsagesOutput[kwhlen - 1]
            console.log(kwhlen)
            var messag = "*" + data.fullName + "*"
            messag += "\nPrepaid "
            messag += data.prepayment ? "✔️" : "✖️"
            messag += `\nAccount balance \t*${data.balance}* Ksh`
            messag += `\n\(_${lastmonthkwh.periodName}_) \t *${lastmonthkwh.consumValue} ${lastmonthkwh.descUnits}*`
            messag += `\nLast Bill (_${lastmonthbill.billingPeriod}_) \t *${lastmonthbill.billAmount} Ksh* `
            messag += `\nBill Balance \t *${lastmonthbill.billPendAmount} Ksh*`

            $.sendMessage(messag, { parse_mode: 'Markdown' })
        }, error => {
            $.sendMessage(error)
        });

    }
    get routes() {
        return {
            'startCommand': 'start'
        }
    }
}
function gettukfees($, id) {
    console.log("The id is ", id)
    var toolongtimeout = setTimeout(() => {
        $.sendMessage("Its taking longer than i expected ...")
    }, 35000)
    var feature = "tuk"
    us.updateFeatureUsed($, feature, data => {
        console.log(`${feature} used: `, data)
    });

    $.sendMessage("A moment, I'm working on your request!");
    tuk.getFees(id, resp => {
        console.log("success", resp)
        clearTimeout(toolongtimeout);

        $.sendMessage(resp.message, { parse_mode: 'Markdown' }).then(res => {

        }, error => {
            console.log(error)
        })
        var ys = "yes (" + resp.pdf + ")"

        if (resp.pdf != "pdf/KENYA") {
            $.runMenu({
                message: 'Should i send the statement  pdf ?',
                oneTimeKeyboard: true,
                options: {
                    parse_mode: 'Markdown' // in options field you can pass some additional data, like parse_mode
                },
                "Yes": () => {
                    console.log("The user said yes")

                    var file = "pdf/MWANGI_113_00421.pdf"
                    $.sendDocument({ path: resp.pdf })
                },
                'No': () => { //will be executed at any other message
                    console.log("The user said no")
                    $.sendMessage("Glad i could help. 😊")
                }
            })

        }


    }, error => {
        console.log("Error", error)
        $.sendMessage("Unable to fetch your details .")


    })
}
class TukgetController extends TelegramBaseController {
    /**
     * @param {Scope} $
     */
    gtfees($) {
        var id = $.message.text.match(reg)[1]
        // console.log("Doing the tuka ", $.message.text.match(reg))
        var toolongtimeout = setTimeout(() => {
            $.sendMessage("Its taking longer than i expected ...")
        }, 35000)
        var feature = "tuk"
        us.updateFeatureUsed($, feature, data => {
            console.log(`${feature} used: `, data)
        });

        $.sendMessage("A moment, I'm working on your request!");
        tuk.getFees(id, resp => {
            // console.log("success", resp)
            clearTimeout(toolongtimeout);
            $.sendMessage(resp.message, { parse_mode: 'Markdown' })
            // var ys = "yes (" + resp.pdf + ")"
            $.runMenu({
                message: 'Should i send the statement  pdf ?',
                oneTimeKeyboard: true,
                options: {
                    parse_mode: 'Markdown' // in options field you can pass some additional data, like parse_mode
                },
                "Yes": () => {
                    console.log("The user said yes")
                    var file = "pdf/MWANGI_113_00421.pdf"
                    $.sendDocument({ path: resp.pdf })
                },
                'No': () => { //will be executed at any other message
                    console.log("The user said no")
                    $.sendMessage("Glad i could help. 😊")
                }
            })
        }, error => {
            console.log("Error")
            $.sendMessage("Unable to fetch your details .")
        })
        // $.sendMessage(`student id ${id}`)

    }

    get routes() {
        return {
            'getfees': 'gtfees'
        }
    }
}

tg.router
    .when(new TextCommand('phone', 'pingCommand'), new PingController())
    // .when(new RegexpCommand(regcmd, 'getfees'), new TukgetController())
    .when(new RegexpCommand(kplcregx, 'startCommand'), new KplcController())
    .when(new RegexpCommand(/^cmd\.\d/, 'startCommand'), new TukController())
    .otherwise(new OtherWiseController())


async function getapiV2($, message) {
    const sessionPath = sessionClient.sessionPath(projectId, $.userId.toString());
    const request = {
        session: sessionPath,
        queryInput: {
            text: {
                // The query to send to the dialogflow agent
                text: message,
                // The language used by the client (en-US)
                languageCode: 'en-US',
            },
        },
    };
    const responses = await sessionClient.detectIntent(request);
    return responses
}

function getapiai($, message, callback, error) {
    getapiV2($, message).then(result => {
        callback(result)
    }).catch(err => {
        error(err)
    })
}
function getresponse(userId, message, callb) {
    var url = "http://api.brainshop.ai/get?bid=446&key=9kBuXjectQWEEMt3&uid=" + userId + "&msg=" + message
    var options = {
        url: url,
        headers: {
            'User-Agent': 'request'
        }
    };
    function callback(error, response, body) {
        // console.log(response)
        if (!error && response.statusCode == 200) {
            var info = JSON.parse(body);
            console.log("BrainBot Debug>>>> ", info)
            callb(info["cnt"])
            // console.log(info.stargazers_count + " Stars");
            // console.log(info.forks_count + " Forks");
        }
    }

    request(options, callback);
}
function handleapiairesponse($, message, responses) {
    // console.log(responses); 
    if (responses.length < 0) return
    const response = responses[0]
    console.log(response.queryResult.fulfillmentMessages)
    let responseText = response.queryResult.fulfillmentText;
    let responseData = response.queryResult.payload;
    let messages = response.queryResult.fulfillmentMessages;

    let action = response.queryResult.action;
    let contexts = response.queryResult.outputContexts;
    let parameters = response.queryResult.parameters;
    console.log(action)
    // console.log("handling api.ai response", contexts);
    if (responseText == '' && !isDefined(action)) {
        //api ai could not evaluate input.
        // console.log('Unknown query' + response.result.resolvedQuery);
        $.sendMessage("I'm not sure what you want. Can you be more specific?");

    } else if (isDefined(action)) {
        handleApiAiAction($, message, action, responseText, contexts, parameters);
    }
    else if (isDefined(responseText)) {
        sendapiresponse($, responseText)
    }
}
function makeapiaivoidrequest($, message) {
    getapiai($, reqmessage, resp => handleapiairesponse($, message, resp),
        erro => $.message("Error occured for... `no tuk reg no`")
    )
}
function parsehostel(hs) {

}
function handleApiAiAction($, message, action, responseText, contexts, parameters) {
    //  console.log("handling the action ", contexts);
    switch (action) {
        ////We get the other brain to reply 
        case 'get_tuk_reg_no':
            console.log("Hello Getting the tuk reg no....");
            us.getUserTukRegno($, $.userId, regno => {
                console.log("reg.. no ", regno)
                if (regno == null) {
                    // getresponse($.userId,"no tuk reg no")
                    // getapiai($, message, callback, error)
                    var reqmessage = "no tuk reg no"
                    getapiai($, reqmessage, resp => handleapiairesponse($, reqmessage, resp),
                        erro => $.message("Error occured for... `no tuk reg no`")
                    )
                }
                else {
                    console.log("getting tuk fees ...")
                    gettukfees($, regno);
                    $.sendMessage("getting fee balance for " + regno)
                }

            }, error => {
                $.sendMessage("Registration number not found...")
                console.log("No reg found...")
            });

            break;
        case 'parse_reg_no':
            console.log("parse_reg_no >>>", parameters)
            //check if the registration number is available
            var reg = parameters.fields.reg_no.stringValue

            if (reg) {
                us.setUserTukRegno($, $.userId, reg, data => {
                    console.log("Update the registration numer ,", data)

                    gettukfees($, reg);
                    $.sendMessage("In case you need to update your reg number, tell me.")
                }, error => {

                });
            }
            break;
        case 'initiate_payment':
            console.log("Initiating pament... ", parameters);
            if (contexts.length == 0) {
                if (parameters.phone[0] == "0") {
                    parameters.phone = "254" + parameters.phone.slice(1)
                }
                payments.initiatePayment(parameters, success => {
                    $.sendMessage("Payment being processsed..");
                }, error => {
                    $.sendMessage("An error occured...")
                })
            }
            else {
                $.sendMessage(responseText)
            }
            break;
        case 'search_hostel':
            $.sendMessage(responseText)

            const parsedParams = parseDialogFlowParameters(parameters)
            if (contexts.length == 0) {
                console.log("Search hostesls>>>> ")
                room.search(parsedParams, hostels => {
                    console.log("The hostels>>>> ")
                    if (hostels.length == 0) {
                        $.sendMessage("No hostels found for those filters.")
                    }
                    else {
                        let name = hostels.length > 1 ? "hostels" : "hostel"
                        let hostel_names = hostels.slice(0, 5).map(hs => {
                            return `\n*${hs.building_name}* (*Ksh ${numeral(hs.rent).format()}* per Person)\n${hs.sharing} sharing* ${hs.location_name}*`
                            // return `
                            // <a href="tg://user?id=384922104">@creator</a>
                            //     `

                        });
                        // hostel_names=hostel_names.replace(","," ")
                        $.sendMessage(`Found ${hostels.length} ${name} ${hostel_names.join(" ")}`, { parse_mode: 'Markdown' });
                        // $.sendMessage(`Found ${hostels.length} ${name} ${hostel_names}`, { parse_mode: 'HTML' });
                    }



                }, error => {
                    console.log("Seach hosetsls error ", error)
                })
            }
            // $.sendMessage(responseText)
            break;

        case 'input.unknown':
            console.log("Unknown api.ai response .", action, contexts, parameters)
            // $.sendMessage("Old Brain");
            getresponse($.userId, message, resp => {
                var feature = "meiu"
                us.updateFeatureUsed($, feature, data => {
                    console.log(`${feature} used: `, data)
                });

                // console.log("teh response ", resp)
                $.sendMessage(resp)

                var conv = { "user": message, "bot": resp }
                conv.time = new Date().toString()
                us.saveUserMessage($, $.userId, conv, success => {
                    console.log("Saved user message");
                })
            })
            break;

        default:
            //unhandled action, just send back the text
            var feature = "api.ai"
            console.log("Not the default this time ...")
            var resp = "_api.ai_\n"
            us.updateFeatureUsed($, feature, data => {
                console.log(`${feature} used: `, data)
            });
            sendapiresponse($, responseText)
    }

}


function sendapiresponse($, responseText) {
    var resp = "➡️ `api.ai`\n\n"
    resp += responseText
    $.sendMessage(responseText, { parse_mode: "Markdown" });
}
function parseDialogFlowParameters(parameters) {
    // console.log("parameters", parameters.fields.location)
    // console.log("parameters2")
    // console.log("parameters", parameters.fields.max_rent)
    let queryParameters = []
    const fields = parameters.fields
    for (let field in fields) {
        let value = fields[field]
        switch (value.kind) {
            case 'stringValue':
                if (value.stringValue == "") break;
                queryParameters.push(`${field}=${value.stringValue}`)
                break;
            case 'numberValue':
                if (value.stringValue == "") break;
                queryParameters.push(`${field}=${value.numberValue}`)
                break;
            case 'listValue':
                try {
                    fields[field].listValue.values.map(val => {
                        let fieldValue = getParameterFieldValue(val)
                        if (fieldValue)
                            queryParameters.push(`${field}=${fieldValue}`)
                    })
                } catch (error) {
                    console.log(error);

                }
                break;
            default:
                break;
        }
    }
    console.log(queryParameters)
    return queryParameters.join("&")
}
function getParameterFieldValue(field) {
    let val = undefined
    switch (field.kind) {
        case 'stringValue':
            if (field.stringValue == "") break;
            val = field.stringValue
            break;
        case 'numberValue':
            if (field.numberValue == "") break;
            val = field.numberValue
            break;
        default:
            break;
    }
    return val
}

function isDefined(obj) {
    if (typeof obj == 'undefined') {
        return false;
    }

    if (!obj) {
        return false;
    }

    return obj != null;
}

