var request = require("request");

module.exports = {
    getToken: (callback, error) => {
        gettoken(data => {
            callback(data)
        }, erro => {
            error(erro)
        });
    },
    getKplcBill: (accountNo, callback, error) => {
        getkplbill(accountNo, data => {
            callback(data)
        }, erro => {
            error(erro)
        })
    }
}

function getkplbill(accountNo, callback, error) {
    ///Get the acccess-token
    gettoken(dat => {

        ///Get the kplc bill
        // console.log(dat)
        getbill(accountNo, dat.access_token, data => {
            if (data != null) {
                callback(data)
            }
        }, erro => {
            error(erro)
        })
    }, erro => {
        error(erro)
    });
}

function getbill(accountid, token, callback, error) {
    // console.log("the token is ", token)
    request({
        uri: "https://selfservice.kplc.co.ke/api/publicData/2.0.1/?accountReference=" + accountid,
        method: "GET",
        headers: {
            Authorization: "Bearer " + token
        }
    }, function (erro, response, body) {
        if (erro != null) {
            error(erro)
        }
        else {
            var resp=JSON.parse(body)
            if ("data" in resp){
                callback(resp.data)
            }
            else{
                error("Account does not exist.")
            }
            
        }
        // console.log(body, erro);
    });
}

function gettoken(callback, error) {
    request({
        uri: "https://selfservice.kplc.co.ke/api/token",
        method: "POST",
        headers: {
            Authorization: "Basic aVBXZkZTZTI2NkF2eVZHc2xpWk45Nl8yTzVzYTp3R3lRZEFFa3MzRm9lSkZHU0ZZUndFMERUdGNh"
        },
        form: {
            grant_type: "client_credentials",
            scope: "token_public accounts_public attributes_public customers_public documents_public listData_public rccs_public sectorSupplies_public selfReads_public serviceRequests_public services_public streets_public supplies_public users_public workRequests_public publicData_public juaforsure_public"
        }
    }, function (erro, response, body) {
        //  console.log(body, erro);
        if (erro != null) {
            error(erro)
        }
        else {
            callback(JSON.parse(body))
        }

    });
}
