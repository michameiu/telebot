# Meiu Bot
## Telegram intergration

###Live Demo


![Meiu Profile Pic](https://cdn4.telesco.pe/file/X52svvO9EUk1-CRruUzfoxypkmr1azY1YNM_AqrQTs6OQtA7T_8SiqyYFuBmaxgFHnVlK_Sr5_rXaLwHUFbDY7VOfZkrdXyOW1K_prDKWbcYIfDVYaQbqvZa01xPGhuF7d-24tOr5thEXZmUrIc1eKBl7FTH4xnwX05euOXBESxIAhrxPuLjWpn0irLbKwAGcXsdsH24aNX1b08a6havQ3w72vE3ufYRkSJj2JG2cO-6NuDGGwjWS0cmIzrfndY2SycVIpf-RpAd1q4orRup45eNM057XE7MRiXfHrgATwvSYSY1_iIYpai8jBgJgVmF8JtmViVB8S9XG80t9jw9_Q.jpg)

https://t.me/meiubot


### Functionalities

* Kplc Bill requesting

    `user> kplc <meter number> `
    
    Response :

        GXXXXX KXXXXXX NXX
        Account balance  -54.05 Ksh
        (10 - October 2018)   0 kWh
        Last Bill (10 - October 2018)   0 Ksh 
        Bill Balance   0 Ksh 


* Technical University of Kenya fees statement.

    `user> tuk <Student registration number>`

    Response:

        KXXXX MXXX MXXXXX :exclamation: 
        Department: ELECTRICAL AND POWER ENGINEERING
        Current Semester: Graduate Convocation Fee December 2018
        You owe the school : Ksh:4995.0
        Fee Balance: 4995
        

* General conversation.

    `user> hi meiu ? `

    `bot> Sema micha.`