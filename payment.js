var request = require("request");

module.exports = {
    initiatePayment: (args, callback, error) => {
        roometoapirequest(args, success => {
            callback(success)
        }, failed => error(failed))
    }
}

function roometoapirequest(data, callback, error) {
    console.log(data);
    request.post({
        url: "https://d93c767d.ngrok.io/botpay",
        form: data
    }, function (erro, response, body) {
        if (erro != null) {
            error(erro)
        }
        else {
            console.log(body)
            var resp = JSON.parse(body)
            callback(resp)
        }
    });
}