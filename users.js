var firebase = require("firebase");
const request = require('request');
// const config = require("./config")
var moment = require("moment");
const numeral = require('numeral');
var foods
// var admin = require("firebase-admin");
// var serviceAccount = require("./firebase-admin.json");
firebase.initializeApp({
    serviceAccount: "./firebase-admin.json",
    //   credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://expatafric.firebaseio.com"
});
var ref = firebase.database().ref()

ref.child("orders").once("value", snaps => {
    snaps.forEach(snap => {
        //console.log(snap.val())
    });
}, onerror => {
    // //console.log(onerror)
});

module.exports = {
    getUser: ($, userId, callback) => {
        firebasegetuser($, userId, user => {
            callback(user)
        });
    },
    getAllUsers: ($, callback, error) => {
        firebaseGetAllusers($, results => callback(results),
            err => error(err))
    },
    updateUserPhone: ($, userId, phone, callback) => {
        firebaseupdateUserPhone($, userId, phone, user => {
            callback(user)
        });
    },
    saveUser: ($, user, callback) => {
        storagesaveuser($, user, data => {
            callback(data);
        });
    },
    saveUserMessage: ($, user, message, callback) => {
        // $, user,message, callback
        storagesaveusermessage($, user, message, success => {
            callback(success)
        })
    },
    updateFeatureUsed: ($, feature, callback) => {
        storageaddfeatureused($, feature, resp => {
            callback(resp);
        })
    },
    getUserTukRegno: ($, user, callback, error) => {
        storagegetusertukregno($, user, callb => {
            callback(callb)
        }, err => error(errr))
    },
    setUserTukRegno: ($, user, tukregno, callback, error) => {
        storagesetusertukregno($, user, tukregno, callb => {
            callback(callb)
        }, err => error(errr))
    }


}

function storagegetusertukregno($, user, callback, error) {
    ref.child("botusers").child($.userId).child("tukregno").once("value", data => {
        console.log("saving the user ...", $.userId)
        callback(data.val());
    }, erro => {
        console.log(erro)
        error(erro)
    })
}

function storagesetusertukregno($, user, tukregno, callback, error) {
    ref.child("botusers").child($.userId).child("tukregno").set(tukregno).then(data => {
        console.log("Saving User registration number ...", $.userId)
        console.log(data);
        callback(data);
    }, erro => {
        console.log("error");
        error(error)
    });
}
function storageaddfeatureused($, feature, callback) {
    var date = new Date()
    //1/2/2017:hr
    if ($.userId == 384922104) {
        callback("Admin using ... ", $.userId)
        return 0;
    }
    date = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}:${date.getHours()}`
    ref.child("featuresStats").child(feature).child(date).once("value", snap => {
        var stat = 0;
        if (snap == null) {

        }
        else {
            stat = snap.val() + 1
        }
        ref.child("featuresStats").child(feature).child(date).set(stat).then(() => {
            callback(stat)
        })
    });
}
function storagesaveuser($, user, callback) {
    ref.child("botusers").child($.userId).update(user).then(data => {
        console.log("saving the user ...", $.userId)
        callback(user);
    })
}
function storagesaveusermessage($, user, message, callback) {
    ref.child("botusersmessages").child($.userId).push(message).then(data => {
        // console.log("saving the user ...", $.userId)
        callback(user);
    })
}
function firebaseGetAllusers($, callback, error) {
    ref.child("botusers").once("value", snaps => {
        let keys = []
        snaps.forEach(snap => {
            keys.push(snap.key)
        })
        callback(keys)
    }, err => error(err))
}
function firebaseupdateUserPhone($, userId, phone, callback) {
    ref.child("botusers").child(userId).update({ "phone": phone })
        .then(() => {
            callback(phone)
        })
}
function firebasegetuser($, userId, callback) {
    ref.child("botusers").orderByChild("userId").equalTo(userId)
        .limitToLast(1).once("child_added", snap => {
            let ord = snap.val()
            if (ord != null) {
                ord.key = snap.key
                callback(ord)
            }
            else {

                $.getChatMember($.userId).then(daa => {
                    var user = daa.user
                    ref.child("botusers").child(userId).update(user)
                    callback(user)
                });
            }

        });
}




// /*
//     Version 2 (Munchbot)
// */

// function firebasegetlatestorder(userId, sessionId, callback) {
//     ref.child("confirmedorders").orderByChild("userId")
//         .limitToLast(1).once("child_added", snap => {
//             let ord=snap.val()
//             ord.key=snap.key
//             callback(ord)
//         });
// }

// function firebaseupdatepayment(refa, status, callback) {
//     if (refa != undefined && status != undefined) {
//         ref.child("confirmedorders").child(refa).update({ paymentstatus: status }).then(data => {
//             callback(data)
//         })
//     }
//     else {
//         callback('jj')
//     }


// }
// function firebasegetfoodsbytype(type, callback, error) {
//     if (type == null) {
//         ref.child("foods").once("value", snaps => {
//             // foods = []
//             // snaps.forEach(snap => {
//             //     let fd = snap.val()
//             //     fd.key = snap.key
//             //     foods.push(fd)
//             // });
//             callback(snaps)
//         }, error => {
//             error(error)
//         })
//     }
//     else {
//         ref.child("foods").orderByChild("type").equalTo(type).once("value", snaps => {
//             // foods = []
//             // snaps.forEach(snap => {
//             //     let fd = snap.val()
//             //     fd.key = snap.key
//             //     foods.push(fd)
//             // });
//             callback(snaps)
//         }, error => {
//             error(error)
//         })
//     }
// }
// function savecalculateorder(sender, sessionId, parameters, callback) {
//     var foods = parameters.LunchOrder ? parameters.LunchOrder : []
//     var quantities = parameters.order_quantity ? parameters.order_quantity : []
//     if (foods.length == 0 || foods.length == quantities.lenth) {
//         callback("FOods not the same, " + foods.join() + " " + quantities.join() + " " + foods.length + " " + quantities.length)
//     }
//     else {
//         parameters2order(foods, quantities, sender, sessionId, data => {
//             var ord = data
//             ord.text = order2text(ord)
//             callback(ord.text)
//         });

//     }
// }
// function firebasegetconfirmedorder(userId, sessionId, callback, error) {

//     ref.child("confirmedorders").orderByChild("userId").equalTo(userId).limitToLast(1).once("value", snaps => {
//         // console.log(snaps)
//         var ord = {}
//         snaps.forEach(snap => {
//             ord = snap.val()
//             ord.ckey = snap.key
//         })
//         // var snap = snaps.length > 0 ? snaps[0] : {}

//         if (!ord.total) {
//             error(undefined)
//         } else {
//             callback(ord)
//         }
//     });
// }
// function order2text(order) {
//     return "totaling " + order.display_currency + " " + numeral(order.total).format('0,0.00')
// }


// function parameters2order(foods, quantities, sender, sessionId, callback) {
//     var total = 0
//     var fds = []
//     var order_foods = []
//     var order = {}
//     order.name = "Lunch " + moment().format('MMMM Do YYYY, h:mm:ss a')
//     order.date = moment().format('MMMM Do YYYY, h:mm:ss a')
//     ref.child("foods").once("value", snaps => {
//         snaps.forEach(snap => {
//             var fd = snap.val()
//             fd.key = snap.key
//             fds.push(fd)
//         })
//         for (var c in foods) {
//             var order_food = {}
//             var f = foods[c]
//             var quantity = quantities[c]
//             var famount = 0
//             if (fds.map(d => d.name).indexOf(f) != -1) famount = fds[fds.map(d => d.name).indexOf(f)]
//             let food = famount
//             // food.name = f
//             order.currency = famount.currency
//             order.display_currency = famount.display_currency
//             famount = famount.amount
//             food.price = famount
//             food.quantity = quantity
//             food.amount = famount * quantity
//             total += famount * quantity
//             order_foods.push(food)
//         }
//         order.total = total
//         order.currency = "TZS"

//         order.userId = sender
//         order.sessionId = sessionId
//         order.foods = order_foods
//         ref.child("orders").child(sessionId).update(order).then((data) => {
//             callback(order)
//         });
//     });

// }

// /*
//     Version 1 (Munchbot_test)
// */
// function getRandomInt(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
// }
// function firebasegetconfirmedorderid(refa, callback) {
//     ref.child("confirmedorders/" + refa).once("value", snap => {
//         callback(snap.val())
//     }, error => {
//         callback(JSON.stringify(error))
//     });
// }

// function firebaseconfirmorder(userId, sessionId, request, callback) {
//     ref.child("orders/" + sessionId).once("value", snap => {
//         let order = snap.val() ? snap.val() : {}
//         order.request = request
//         ref.child("users").child(userId).once("value", snaap => {
//             order.user = snaap.val()
//             var de = ref.child("confirmedorders").push(order)
//             console.log(de)
//             order.key = de.key
//             console.log("the order is ", order)
//             callback(order)
//         });
//     }, error => {
//         console.log(error);
//     })
// }

// function formatorder(order) {
//     let mes = ""
//     // //console.log(order.drinks);
//     dish = order.dish.international_dishes ? order.dish.international_dishes : order.dish.african_dishes;
//     drinks = order.drinks ? getdrinks(order.drinks).join() : ' No drinks '
//     mess = `Here is your order ${dish} with ${drinks} . \nShould I place the order for you ?`
//     return mess;
// }
// function getdrinks(drinks) {
//     let drs = []
//     drinks.forEach(function (value, keay) {
//         for (key in value) {
//             drs.push(value[key])
//         }
//     })
//     // //console.log(JSON.stringify(drs))
//     return drs
// }
// function firebasegetorder(userId, sessionId, callback) {
//     ref.child("orders/" + userId + "/" + sessionId).once("value", snap => {
//         callback(snap.val())
//     }, error => {
//         //console.log(error);
//     })
// }
// function firebaseupdateorder(userId, sessionId, order, callback) {
//     ref.child("orders/" + userId + "/" + sessionId).update(order).then(data => {
//         callback(data)
//     });
// }
// function firebasegetuser(userId, success, error) {
//     ref.child("users").child(userId).once("value", (snap) => {
//         if (snap.val() == null) {
//             getuserfromfb(userId, (user) => {
//                 success(user)
//             });
//         }
//         else {
//             success(snap.val())
//         }
//     }, serror => {
//         //console.log(serror);
//         error(serror)
//     });
// }
// function getuserfromfb(userId, callback) {
//     request({
//         uri: 'https://graph.facebook.com/v2.7/' + userId,
//         qs: {
//             access_token: config.FB_PAGE_TOKEN
//         }
//     }, function (error, response, body) {
//         if (!error && response.statusCode == 200) {
//             var user = JSON.parse(body);
//             //console.log("FB user: %s %s, %s",
//             //  user.first_name, user.last_name, user.gender);
//             ref.child("users").child(userId).set(user)
//             callback(user);
//         } else {
//             //console.error(response.error);
//         }
//     });
// }

