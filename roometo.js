var request = require("request");

module.exports = {
    search: (args, callback, error) => {
        searchhostel(args,
            data => callback(data),
            erro => error(erro)
        )
    }
}

function searchhostel(args, callback, error) {
    roometoapirequest(args, hostels => {
        callback(hostels)
    }, erro => error(erro))
}

function parseQueryParams(params) {
    var res = ""
    for (var key in params) {
        if (params.hasOwnProperty(key)) {
            // console.log(key + " -> " + params[key]);
            if (typeof (params[key]) == "object") {
                for (var l in params[key]) {
                    res += key + "=" + params[key][l] + "&"
                }
            } else {
                res += key + "=" + params[key] + "&"
            }

        }

    }
    return res
}

function roometoapirequest(args, callback, error) {
    var params = args
    console.log(params)
    request({
        uri: "https://api.roometo.com/api/apartments/search?" + params,
        method: "GET"
    }, function (erro, response, body) {
        if (erro != null) {
            error(erro)
        }
        else {
            var resp = JSON.parse(body)
            callback(resp)
        }
    });
}