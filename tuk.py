from bs4 import BeautifulSoup
import requests
import sys
import HTMLParser
import subprocess

import json
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
# fire2=firebase.FirebaseApplication("https://requestv2.firebaseio.com",None)




grad='http://ar.tukenya.ac.ke/img/Graduate-form.pdf'
invoice='http://ar.tukenya.ac.ke/includes/invoice4.php'
# reg=592
# while reg != 593:
#     no='113/01'+str(reg)
#     with requests.session() as c:
#         url='http://ar.tukenya.ac.ke/checklogin.php?action=login'
#         c.post(url,{'PersistentCookie':'yes','username':'112/00768'})
#         pg=c.get("http://ar.tukenya.ac.ke/summary.php")
#         soup=BeautifulSoup(pg.content,'html.parser')
#         names=soup.find_all('h1')[0].text
#         pdf='pdf/'+names.split(' ')[-1]+'_'+no.replace('/',"_")+'.pdf'
#         pds=soup.find_all('tr')
#         #print len(pds)
        
#         if len(pds) > 9:
#             ##print pg.content
#             pd=c.get(invoice)
#             with open(pdf, 'wb') as f:
#                 f.write(pd.content)
        
        
#     reg+=1;

def feestatement(stdno):
    # url='http://ar.tukenya.ac.ke/checklogin.php?action=login'
    # reg=stdno
    # r=requests.post(url,{'PersistentCookie':'yes','username':reg})
    # soup=BeautifulSoup(r.content,'html.parser')
    soup=""
    name=""
    with requests.session() as c:
            url='http://ar.tukenya.ac.ke/checklogin.php?action=login'
            c.post(url,{'PersistentCookie':'yes','username':stdno})
            pg=c.post("http://ar.tukenya.ac.ke/summary.php",{"common_units_skip":"SKIP INTERVIEW"})
            soup=BeautifulSoup(pg.content,'html.parser')
            names=soup.find_all('h1')[0].text
            pdf='pdf/'+names.split(' ')[-1]+'_'+stdno.replace('/',"_")+'.pdf'
            print("pdf."+pdf)
            pds=soup.find_all('tr')
            # print len(pds)
            if len(pds) > 9:
                # print pg.content
                pd=c.get(invoice)
                with open(pdf, 'wb') as f:
                    f.write(pd.content)
            
            

    name=soup.find_all('h1')[0].text
    message=""
    names=[]
    #print(soup)
    i=-1
    table = soup.findAll("table",{"border":"0"})
    #print (len(table))
    if len(table)>0:
        ##print(table[0])
        soup=table[-1]
        names = soup.find_all('tr')
        d=0
        for s in names:
            #print  d,s
            d=d+1
        return parse_border_0(names,name)

    else:
        tabs=soup.findAll("table")
        if (len(tabs) == 0):
            return "\xF0\x9F\x98\xA0 I don't know you (TUK) "
        else:
            return "\xF0\x9F\x98\xAB An error occured \xF0\x9F\x92\xA5 \n *"+name+"* contact developer"
    # for n in names:
    #     #print (d,n)
    #     d=d+1;
    ##print(names[2])
    #for n in names:#print n

    # if len(names) > 5:
    #     fees=names[7].text
    #     f=fees.split('KSH.')
    #     bal=0
    #     message=""
    #
    #     # if(len(f)) > 1:
    #     #     #print len(f)
    #
    #     #     bal=float(f[1])
    #     #     #print("new",bal)
    #     #     message=parsenew(names,bal,name)
    #     # else:
    #     #     fees = names[11].text
    #     #     f = fees.split(':')
    #     #     bal = float(f[1])
    #     #     #print("old", bal)
    #     #     message=parseold(names,bal,name)
    #
    #     return  message
    #     #message += names[4].text + "\n"
    #
    #     # message+=names[6].text+"\n"
    #     # message+=names[10].text+"\n"
    #     # message+=names[28].text+"\n"
    #     # message+=names[11].text+"\n"
    #     ##print message
    #

    message+="Student Id not valid"
    
    ##print message
    return message

def parse_border_0(names,name):
    message = ""
    isdone, mess = balancemessage(names[9].text.split(":")[-1])
    #print isdone
    if isdone:
        message+="*"+name+"* \xE2\x9C\x85 \n"
    else:
        message +=  "*"+name + "* \xE2\x9D\x97 \n"
    message+=names[7].text+"\n"
    message+=names[8].text+"\n"
    message+=mess
    message+="*"+names[9].text+"*\n"
    return message


def parse_gridtable(names):
    message=""
    return message

# def parseold(names,bal,name):
#     #for n in names:#print n
#     message="*"+name+"*\n"
#     message+=balancemessage(bal)
#     message+=names[6].text+"\n"
#     message+=names[10].text+"\n"
#     message+=names[28].text+"\n"
#     message+="*"+names[11].text+"*\n"
#     return message

# def parsenew(names,bal,name):
#     message=""
#     message += "*" + names[6].text + "*\n"
#     message += names[5].text + "\n\n"
#     message+=balancemessage(bal)
#     message += "Fee balance: *Ksh " + str(bal) + "*"
#     return  message


##Check the balance
def balancemessage(bal):
    bal=float(bal)
    message=""
    isdone=False
    if bal == 0:
        mes = 'Fees Cleared\n'
        message = mes + "\n"
        isdone=True
    elif bal > 0:
        isdone = False
        mes = 'You owe the school\n'
        message += mes + " \xF0\x9F\x92\xB0 *Ksh:" + str(bal) + "*\n"
    else:
        isdone = True
        mes = 'The school owes you '.encode("utf-8")
        message += mes + " \xF0\x9F\x92\xB0  *Ksh:" + str((-bal)) + "*\n"
    return isdone,message


def ping(sHost):
    try:
        output = subprocess.check_output("ping -c 1 "+sHost, shell=True)
    except Exception, e:
        return False

    return True
def requeststatus(server):
    az1="https://"+server+".rquest.co/upload"
    r=requests.get(az1)
    return "Server "+server+" Running " if r.status_code == 200 else "Server "+server+" Down"

# def request():
#      message="GENERAL\n"
#      gel=fire2.get('/general/',None)
#      for k,v in gel.iteritems():
#          message+=k+"--------> "+str(v)+" \n"
    
#      gen=fire2.get('/genres/',None)
#      strin="\nGENRES\n"
#      for k,v in gen.iteritems():
#          strin+=v["name"]+"---------> "+str(v["plays"])+" plays\n"
#      message+=strin
#      return message
def read_in():
    lines = sys.stdin.readlines()
    # print(lines)
    # Since our input would only be having one line, parse our JSON data from that
    return lines[0].strip()

def chat(mess,id):
    res= "http://api.acobot.net/get?bid=446&key=9kBuXjectQWEEMt3&uid="+id+"&msg="+mess
    resp=json.loads(requests.get(res).content)
    return resp["cnt"]
#
if __name__ == "__main__":
    id=None
    id=read_in()
    # print(id)
    gid=id if id !=None else "113/00421" 
    name=feestatement(gid)
    print(name)

